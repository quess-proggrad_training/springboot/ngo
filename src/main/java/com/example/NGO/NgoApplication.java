package com.example.NGO;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class NgoApplication {

	public static void main(String[] args)
	{
		ApplicationContext context= new ClassPathXmlApplicationContext("ngo.xml");
		Check check=context.getBean("ngo", Check.class);
		check.Condition();

	}

}
