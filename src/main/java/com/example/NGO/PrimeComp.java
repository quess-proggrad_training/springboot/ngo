package com.example.NGO;

public class PrimeComp {
    public int n;



    public PrimeComp() {
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
    public void CheckPrime(int nu)
    {
        if(isPrime(nu))
            System.out.println("Prime");
        else
            System.out.println("Composite");
    }
    public static boolean isPrime(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i < Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}
